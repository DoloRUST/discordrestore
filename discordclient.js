const { Client, Intents, WebhookClient } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_WEBHOOKS] });
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const readJson = require('./jsonReader');

const commands = [{
        name: 'poggers',
        description: 'Replies with Pong!',
        reply: 'Poggers!'
    },
    {
        name: 'notpog',
        description: 'Replies with Pong!',
        reply: 'Not very poggers...'
    }
];


async function assignCommands(applicationId, guildId, rest) {
    try {
        console.log('Started refreshing application (/) commands.');

        await rest.put(
            Routes.applicationGuildCommands(applicationId, guildId), { body: commands },
        );

        console.log('Successfully reloaded application (/) commands.');
    } catch (error) {
        console.error(error);
    }
}



function replyToClient(applicationId, token) {
    client.login(token);

    const rest = new REST({ version: '9' }).setToken(token);


    client.on('ready', () => {
        console.log(`Logged in as ${client.user.tag}!`);
        console.log("done");
    });

    client.on('interactionCreate', async interaction => {
        if (interaction.isCommand()) {
            let indexReply = commands.findIndex(x => {
                if (x.name.includes(interaction.commandName.trim())) return true
            });
            console.log(indexReply)
            if (indexReply !== -1) {
                await interaction.reply(commands.at(indexReply).reply)
            }
        }
    });

    client.on('messageCreate', async(message) => {
        if (message.content.includes("?restore channel")) {
            let webhooks = await message.channel.fetchWebhooks()
            let webhook = webhooks.first();

            let jsonToRestore = await readJson.returnJson('./test.json');
            console.log("There are at least " + String(jsonToRestore.messages.length) + " messages");
            let messagesToRestore = jsonToRestore.messages;
            messagesToRestore.forEach((message) => {
                if (message.attachments.length > 0) {
                    var messageSent;
                    var attachmentLinks = "";
                    message.attachments.forEach((attachedFile) => {
                        attachmentLinks += attachments.url + `\n`;
                    });
                    if (message.content.length > 0) {
                        // just send them as messages
                        messageSent = webhook.send({
                            content: message.content + '\n' + attachmentLinks
                            username: message.author.nickname,
                            avatarURL: message.author.avatarUrl
                        }).then(message => {
                            if (message.isPinned) message.pin();
                        });

                    } else {
                        messageSent = webhook.send({
                            content: attachmentLinks,
                            username: message.author.nickname,
                            avatarURL: message.author.avatarUrl
                        }).then(message => {
                            if (message.isPinned) message.pin();
                        });
                    }
                }


            });
        }

    });

}

exports.replyToClient = replyToClient;