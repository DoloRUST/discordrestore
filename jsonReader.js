const fs = require('fs').promises;
const util = require('util');
const { mainModule } = require("process");
const { Console } = require("console");

async function jsonParsing(filename) {
    let resultJson = await fs.readFile(filename, "utf8", (err, jsonString) => {
        if (err) {
            console.log("Error reading file from disk:", err);
            return;
        }
        try {
            return jsonString
            console.log("Json now parsed");
        } catch (err) {
            return;
        }
    })
    if (resultJson == false) {
        return
    } else {
        return JSON.parse(resultJson);
    }
};

async function returnJson(filename) {
    let jsonString = await jsonParsing(filename);
    //while (true) {
    //    if (typeof jsonResult !== "undefined") {
    //        break
    //    }
    //}
    return jsonString;
}

exports.returnJson = returnJson;