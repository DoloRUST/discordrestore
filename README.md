# DiscordRestore

Restore backup made from discord chat exporter from https://github.com/Tyrrrz/DiscordChatExporter

## How to use it
1. Git clone it
2. Run index.js with test.json in the primary file
3. Run `?restore channel` on the channel you want it to restore (ensure that there is a webhook on said channel otherwise it will probably break)
4. Wait for it to restore (calculate the time needed by dividing the amount of messages to 30/minutes)
