const readJson = require('./jsonReader');
const discordclient = require('./discordclient')

let botCredentials = readJson.returnJson('./configbot.json')
botCredentials.then(x => {
    discordclient.replyToClient(x.clientid, x.botToken);
})